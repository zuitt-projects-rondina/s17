/*console.log("hello");*/


// COMMON EXAMPLES OF ARRAY

let grades = [98.5, 94.3, 89.2, 90.1];

let marvelHeroes = ["Iron Man", "Captain America", "Thor", "Hulk", "Black Widow", "Hawkeye", "Shang Chi", "Spiderman"];


//Alternative way to write an ARRAYS

let myTask = [
		"drink HTML",
		"eat Javascript",
		"inhale CSS",
		"bake react"
];

// ARRAYS AND INDEXES
	// [] is called array literals
	// index starts with 0

	// REASSIGN VALUES IN ARRAY
	console.log(myTask);
	myTask[0] = "sleep for 8 hours";
	console.log(myTask);

	// ACCESSING FROM AN ARRAY

	console.log(grades[2]);
			//RESULT : 89.2
	console.log(marvelHeroes[6]);
			// result : Sang Chi
	console.log(myTask[20]);
			// undefined


	// Getting the length of an array
		// we can use .length property to get the number of elements in an array		


	console.log(marvelHeroes.length);
			// result : 8

	if(marvelHeroes.length > 5){
		console.log("We have too many heroes , please contact Thanos")

	};		


	// Accessing the last element of an array

	let lastElement = marvelHeroes.length-1;
	console.log(marvelHeroes[lastElement]);
			//result : Spiderman


	// ARRAY METHODS

		// MUTATOR METHODS
			// functions that mutate or change an array after they're created

	let fruits = ["Apple", "Blueberry", "Orange", "Grappes"];

	// push()
		// adds an element in the end of an array and return the array's length

	console.log(fruits);
	let fruitsLength = fruits.push("Mango");
	console.log(fruits);	


	// ADDING MULTIPLE ELEMENTS

	fruits.push ("Guava", "Kiwi");
	console.log(fruits);

	// pop()
		// removes the last element in an array and returns the removed element

	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log(fruits);
			
	fruits.pop();
	console.log(fruits);

	// unshift()
		// adds an element at the beginning of an array

	fruits.unshift("Guava");
	console.log(fruits);

	fruits.unshift("Kiwi", "Lime");
	console.log(fruits);


	// shift()
		// remove an element at the beginning of an array

	fruits.shift();
	console.log(fruits);	

	/*
		NOTE
		PUSH AND UNSHIFT - ADDING ELEMENTS
		POP AND SHIFT = REMOVE ELEMENTS
	*/


	// splice()
		//simultaneously removes elements from a specified index number and adds elements
		/*
			SYNTAX
			arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
			
		*/

		fruits.splice(1, 2, "Chery", "Watermelon");
		console.log(fruits);



		// removes from index 3 to the last element
		fruits.splice(3);
		console.log(fruits);

		fruits.splice(1,1);
		console.log(fruits);
		//adding in the middle
		fruits.splice(1, 0 , "Cherry", "Buko");
		console.log(fruits);

	// sort()
	// SORT METHOED REARRANGES ELEMENTS IN ALPHANUMERIC ORDER

		fruits.sort();
		console.log(fruits);

	// reverse();
		// reverse the order of the array elements

		fruits.reverse();
		console.log(fruits);


// NON MUTATOR METHODS
		// These functions do not modify or change an array

	let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

// indexOf()
		// return the index number of the first matching element found in an array
		// SYNTAX arrayName.indexOf(searchedValue)
	console.log(countries.indexOf("PH"));
							// result : 1

	let indexOfSG = countries.indexOf("SG");
	console.log(indexOfSG);						
							// result : 3

	let invalidCountry = countries.indexOf("SK");
	console.log(invalidCountry);
							// result : -1

	let invalidCountry2 = countries.indexOf("JP");
	console.log(invalidCountry2);
							// result : -1

	
//slice()
		// slices elements from an array and return new array
		/*
			SYNTAX
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex, endingIndex);

		
		*/											
		// slice off elements from a specified index to the last element
				
	let slicedArrayA = countries.slice(2);
	console.log(slicedArrayA);
						//result ['CAN', 'SG', 'TH', 'PH', 'FR', 'DE']
	console.log(countries);

	//  slice off elements fro, specified index to another index

	let slicedArrayB = countries.slice(2,4);
	console.log(slicedArrayB);
					// result ['CAN', 'SG']

	// slice off elements starting from the last element of an array

	let slicedArrayC = countries.slice(-3);
	console.log(slicedArrayC);
					//result ['PH', 'FR', 'DE']

// toString()
			// returns an array as string seperated by commas
			// SYNTAX : arrayName.toString();

			let stringArray = countries.toString();
			console.log(stringArray);
						// result : US,PH,CAN,SG,TH,PH,FR,DE

			let sentence = ["I", "like", "javascript", ".", "Its", "fun", "!"];

			let sentenceString = sentence.toString();
			console.log(sentenceString);


// concat()
		// combines two array and returns a combined result
		// Syntax : arrayA.concat(arrayB);
				// arrayA.concat(elementA);

		let taskArrayA = ["drink html", "eat javascript" ];
		let taskArrayB = ["inhale CSS", "breathe sass" ];
		let taskArrayC = ["get git", "be node" ];

		let task = taskArrayA.concat(taskArrayB);
		console.log(task);

			// COMBINING MULTIPLE ARRAYS

		let allTask = taskArrayA.concat(taskArrayB, taskArrayC);
		console.log(allTask);

			// COMBINING ARRAY WITH ELEMENTS
		let combinedTask = taskArrayA.concat("smell express", "throw react");
		console.log(combinedTask);


// join()
		// returns an array as a string seperated by a specified seperator string

		let members = ["Rose", "Lisa", "Jisoo", "Jennie"];

		let joinedMembers1 = members.join();
		console.log(joinedMembers1);
					// result : comma as seperator

		let joinedMembers2 = members.join('');
		console.log(joinedMembers2);
					// result : no space as seperator

		let joinedMembers3 = members.join(" ");
		console.log(joinedMembers3);
					// result: space as sperators

		let joinedMembers4 = members.join("/");
		console.log(joinedMembers4);
					// result: / as sperators



// ITERATION METHODS
		// they are loops design to perform repetitive task on arrays
		// useful for manipulating array data resulting in complex tasks



// forEach()

	// an empty array will stored the filtered elements in the iteration method
	// This is to avoid confusion by modifying the original array
	// have a function 
	let filteredTask = [];
	allTask.forEach(function(task){

			console.log(task);

			if(task.length > 10){

				filteredTask.push(task);

			};
	});

	console.log(filteredTask);


// map()
		// iterates on each element and returns new array with different values
		// unlike forEach method, map method requires the use of return keyword in order
		// order to create another array with the performed operation

	let numbers = [1, 2, 3, 4, 5];

	let numberMap = numbers.map(function(number){
					return number * number ;
	});

	console.log(numberMap);


// every()
		// check if ALL elements meet a certain condition
		// returns true value if all elements meet the condition otherwise false


	let allValid = numbers.every(function(number){
		return (number < 3)
	});

	console.log(allValid);
				// return false

// some()
		//checks if atleast ONE element in the array that meet the condition
		// returns true value if one elements meet the condition otherwise false
		let someValid = numbers.some(function(number){
			return (number < 2);
		});

		console.log(someValid);
			// return false

// fiter()
		// return a new array that contains the elements that meet a certain condition
		// if there is NO elements that did not meet condition will return an empty array
		let filterValid = numbers.filter(function(number){
				return (number < 3);
		});

		console.log(filterValid);
				// result [1, 2]


		let colors = ["red", "green", "black", "orange", "yellow"];

		let values = ["red", "black", "yellow"];





		colors = colors.filter(item => values.indexOf(item) === -1)

		console.log(colors);


		/*
				colors = colors.filter(function(item){
					return (values.indexOf(item) === -1)
				})
			*/

// includes()
			// returns a boolean value true if it finds a matching item in the array
			//includes is case-sensitive


		let products = ["Mouse", "Keyboard", "Laptop", "Monitor"]

		let filteredProducts = products.filter(function(product){

				return product.toLowerCase().includes("a");

		});

		console.log(filteredProducts);

// MULTIDIMENSIONAL ARRAY
				// useful for storing complex data structures 

	let chessboard = [
				["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
				["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
				["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
				["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
				["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
				["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
				["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
				["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
	];

	console.log(chessboard[1][4]);

